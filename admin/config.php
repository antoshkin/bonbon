<?php
// HTTP
define('HTTP_SERVER', 'http://bon-bon.shop/admin/');
define('HTTP_CATALOG', 'http://bon-bon.shop/');

// HTTPS
define('HTTPS_SERVER', 'http://bon-bon.shop/admin/');
define('HTTPS_CATALOG', 'http://bon-bon.shop/');

// DIR
define('DIR_APPLICATION', 'C:/Projects/bonbon/admin/');
define('DIR_SYSTEM', 'C:/Projects/bonbon/system/');
define('DIR_IMAGE', 'C:/Projects/bonbon/image/');
define('DIR_LANGUAGE', 'C:/Projects/bonbon/admin/language/');
define('DIR_TEMPLATE', 'C:/Projects/bonbon/admin/view/template/');
define('DIR_CONFIG', 'C:/Projects/bonbon/system/config/');
define('DIR_CACHE', 'C:/Projects/bonbon/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/Projects/bonbon/system/storage/download/');
define('DIR_LOGS', 'C:/Projects/bonbon/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/Projects/bonbon/system/storage/modification/');
define('DIR_UPLOAD', 'C:/Projects/bonbon/system/storage/upload/');
define('DIR_CATALOG', 'C:/Projects/bonbon/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'bonbonshop');
define('DB_PASSWORD', 'gTRk38>,sh^&CFSI7gt6518');
define('DB_DATABASE', 'bonbon');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
