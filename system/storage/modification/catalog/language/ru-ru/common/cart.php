<?php

        // oct_techstore start
        $_['text_cart_heading'] = 'Корзина:';
        $_['text_cart_items']     = '<span class="cart-total-price">%s</span> <i class="fa fa-shopping-cart"><span class="count-quantity">%s</span></i>';
        // oct_techstore end
      
// Text
$_['text_items']    = 'Товаров %s (%s)';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Перейти в корзину';
$_['text_checkout'] = 'Оформить заказ';
$_['text_recurring']  = 'Платежный профиль';

